import { ActivatedRoute } from '@angular/router';
import { GithubService } from './../services/github.service';
import { Http } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'followers',
    templateUrl: './followers.component.html',
    styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {

    followers;
    constructor(private service: GithubService, private route: ActivatedRoute) { }

    ngOnInit() {
        Observable.combineLatest([
            this.route.paramMap,
            this.route.queryParamMap
        ])
            .switchMap(combine => {
                var id = combine[0].get('id');
                var id = combine[1].get('page');
                // this.service.getAll({ id: id, page: page });
                return this.service.getAll()
            })
            .subscribe(followers => this.followers = followers);

    }

}
