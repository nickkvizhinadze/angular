import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'tweet',
    templateUrl: './tweet.component.html',
    styleUrls: ['./tweet.component.css']
})
export class TweetComponent {
    @Input() tweet: Tweet;

    constructor() { }

    onClick() {
        this.tweet.isLiked = !this.tweet.isLiked;
        this.tweet.likesCount += this.tweet.isLiked ? 1 : -1;
    }

}


export interface Tweet {
    body: string,
    isLiked: boolean,
    likesCount: number
}