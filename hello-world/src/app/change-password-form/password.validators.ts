import { FormControl, AbstractControl, ValidationErrors } from "@angular/forms";

export class PasswordValidators {
    static oldPasswordCheck(control: AbstractControl): Promise<ValidationErrors | null> {
        return new Promise((resolve, refuse) => {
            setTimeout(() => {
                if (control.value === '1234')
                    resolve(null);
                else
                    resolve({ oldPasswordCheck: true });
            }, 2000);
        });
    }

    static passwordShouldMatch(control: AbstractControl): ValidationErrors | null {
        let newPassword = control.get('newPassword');
        let confirmPassword = control.get('confirmPassword');
        if (newPassword.value === confirmPassword.value)
            return null;
        else
            return { passwordShouldMatch: true };
    }
}