import { debug } from 'util';
import { BadRequestError } from './../common/bad-request-error';
import { AppError } from './../common/app-error';
import { NotFoundError } from './../common/not-found-error';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class GithubService {
    url = 'https://api.github.com/users/NickKvizhinadze/followers';
    constructor(private http: Http) { }

    getAll() {
        return this.http.get(this.url)
            .map(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error) {
        switch (error.status) {
            case 404:
                return Observable.throw(new NotFoundError());
            case 400:
                return Observable.throw(new BadRequestError(error.json()));
            default:
                return Observable.throw(new AppError(error));
        }
    }
}