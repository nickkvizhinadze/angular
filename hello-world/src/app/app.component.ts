import { Component } from '@angular/core';
import { Tweet } from './tweet/tweet.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = '';

    tweet:Tweet = {
        body: "Here is a body of the tweet...",
        isLiked: false,
        likesCount: 0
    }

    OnKeyUp() {
        console.log("here");
    }
}
