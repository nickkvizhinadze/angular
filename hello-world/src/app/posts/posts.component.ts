import { BadRequestError } from './../common/bad-request-error';
import { AppError } from './../common/app-error';
import { Component, OnInit } from '@angular/core';
import { promise } from 'protractor';
import { PostService } from './../services/post.service';
import { NotFoundError } from '../common/not-found-error';

@Component({
    selector: 'posts',
    templateUrl: './posts.component.html',
    styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
    posts;

    constructor(private service: PostService) {
    }

    ngOnInit() {
        this.service.getAll()
            .subscribe(posts => {
                this.posts = posts;
            });
    }

    createPost(input: HTMLInputElement) {
        let post = { title: input.value };
        this.service.create(post)
            .subscribe(nwePost => {
                post['id'] = nwePost.id;
                this.posts.splice(0, 0, post);
                input.value = '';
            }, (error: AppError) => {
                if (error instanceof BadRequestError) {
                    // this.form.setErrors(error.json());
                }
                else
                    throw error;
            });
    }

    updatePost(post) {
        this.service.update(post.id, { isRead: true })
            .subscribe(updatedPost => {
                console.log(updatedPost);
            });
    }

    deletePost(post) {
        this.service.delete(post.id)
            .subscribe(deletedPost => {
                let index = this.posts.indexOf(post);
                this.posts.splice(index, 1);
            }, (error: AppError) => {
                if (error instanceof NotFoundError)
                    alert('This post has already been deleted.');
                else
                    throw error;
            });
    }

}
