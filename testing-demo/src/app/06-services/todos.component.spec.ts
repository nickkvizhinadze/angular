import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/throw';

import { TodosComponent } from './todos.component';
import { TodoService } from './todo.service';

describe('TodosComponent', () => {
  let component: TodosComponent;
  let service: TodoService;

  beforeEach(() => {
    service = new TodoService(null);
    component = new TodosComponent(service);
  });

  it('should set todos property with the items returned from server', () => {
    const todos = [1, 2, 3];
    spyOn(service, 'getTodos').and.callFake(() => {
      return Observable.from([todos]);
    });

    component.ngOnInit();

    expect(component.todos).toBe(todos);
  });

  it('should call the server to save the changes when a new todo is added', () => {
    const spy = spyOn(service, 'add').and.callFake(() => {
      return Observable.empty();
    });
    component.add();

    expect(spy).toHaveBeenCalled();
  });

  it('should add new todo returned from server', () => {
    const todo = { id: 1, name: 'a' };
    const spy = spyOn(service, 'add').and.returnValue(Observable.from([todo]));
    component.add();

    expect(component.todos).toContain(todo);
  });

  it('should set a message property if server returns an error when adding new todo', () => {
    const error = 'error from the server';
    const spy = spyOn(service, 'add').and.returnValue(Observable.throw(error));
    component.add();

    expect(component.message).toBe(error);
  });
});