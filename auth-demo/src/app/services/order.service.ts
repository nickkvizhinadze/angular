import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class OrderService {

  constructor(private http: Http, private authHttp: AuthHttp) {
  }

  getOrders() {
    //Authorization useage with standart http
    // let token = localStorage.getItem('token');
    // let headers = new Headers();
    // headers.append('Authorization', 'Bearer ' + token);

    // let options = new RequestOptions({ headers: headers })

    // return this.http.get('/api/orders', options)
    //   .map(response => response.json());

    //Authorization useage with standart auth http(jwt)
    return this.authHttp.get('/api/orders')
      .map(response => response.json());
  }
}
