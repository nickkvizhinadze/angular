import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import { MatDialog } from '@angular/material';
import { EditCourseComponent } from './edit-course/edit-course.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  progress = 0;
  timer;
  isLoading = false;

  constructor(private dialog: MatDialog) {
    // Progressbar
    this.timer = setInterval(() => {
      this.progress++;
      if (this.progress === 100) {
        clearInterval(this.timer);
      }
    }, 20);

    this.isLoading = true;
    this.getCourses()
      .subscribe(() => this.isLoading = false);
  }
  // Checkbox
  isChecked = true;

  // Dropdown list
  colors = [
    { id: 1, name: 'Red' },
    { id: 2, name: 'Green' },
    { id: 3, name: 'Blue' }
  ];

  color = 2;

  // Datepicker
  minDate = new Date(2018, 0, 1);
  maxDate = new Date(2018, 7, 1);

  // Chips
  categories = [
    { name: 'Beginner' },
    { name: 'Intermediate' },
    { name: 'Advanced' }
  ];

  selectCategory(category) {
    this.categories
      .filter(c => c !== category)
      .forEach(c => c['selected'] = false);
    category.selected = !category.selected;
  }

  // ProgressBar
  getCourses() {
    return Observable.timer(2000);
  }

  // Dialog
  openDialog() {
    this.dialog
      .open(EditCourseComponent, {
        data: { courseId: 1 }
      })
      .afterClosed()
      .subscribe(result => console.log(result));
  }

  // Checkbox change
  onChange($event) {
    console.log($event);
  }
}
