import { Component } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from '@firebase/util';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    courses$;
    course$;
    author$;

    constructor(private db: AngularFireDatabase) {
        this.courses$ = db.list('/courses').valueChanges();
        this.course$ = db.object('/courses/1').valueChanges();
        this.author$ = db.object('/authors/1').valueChanges();
    }

    add(course: HTMLInputElement) {
        this.db.list('/courses').push({
            name: course.value,
            price: 150,
            isLive: true,
            sections: [
                { title: 'Components' },
                { title: 'Directives' },
                { title: 'Templates' },
            ]
        });
        course.value = '';
    }

    update(course) {
        console.log(course.key);

        // this.db.object('/courses/' + course.key)
        //     .set(course.$value + 'updated');
    }

    delete(course) {
        this.db.object('/courses/' + course.$key)
            .set(course.$value + 'updated');
    }
}
