// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyDANGQWMWpTZHHB4IIkxP_ZuJYL58cU9Y4',
        authDomain: 'fir-demo-ebd7b.firebaseapp.com',
        databaseURL: 'https://fir-demo-ebd7b.firebaseio.com',
        projectId: 'fir-demo-ebd7b',
        storageBucket: 'fir-demo-ebd7b.appspot.com',
        messagingSenderId: '988901873643'
    }
};
