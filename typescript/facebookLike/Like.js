"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Like = /** @class */ (function () {
    function Like(_count) {
        this._count = _count;
    }
    Object.defineProperty(Like.prototype, "count", {
        get: function () {
            return this._count;
        },
        set: function (value) {
            this._count = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Like.prototype, "selected", {
        get: function () {
            return this._selected;
        },
        set: function (value) {
            this._selected = value;
        },
        enumerable: true,
        configurable: true
    });
    Like.prototype.click = function () {
        if (!this._selected) {
            this._count++;
        }
        else {
            this._count--;
        }
        this._selected = !this._selected;
    };
    return Like;
}());
exports.Like = Like;
