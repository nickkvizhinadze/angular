export class Like {
    private _selected: boolean;

    constructor(private _count: number) {
    }

    get count() {
        return this._count;
    }

    get selected() {
        return this._selected;
    }

    click() {
        this._count += (this._selected) ? -1 : 1;
        this._selected = !this._selected;
    }
}