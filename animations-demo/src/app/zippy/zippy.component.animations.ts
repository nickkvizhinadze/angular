import {
    trigger,
    state,
    transition,
    style,
    animate,
    keyframes,
    useAnimation,
    query,
    animateChild,
    group,
    stagger
} from '@angular/animations';

export const expandeCollapse = trigger('expandCollapse', [
    state('collapsed', style({
        height: 0,
        paddingTop: 0,
        paddingBottom: 0,
        opacity: 0
    })),
    transition('collapsed => expanded', [
        animate('300ms ease-out', style({
            height: '*',
            padding: '*'
        })),
        animate('0.8s', style({
            opacity: 1
        }))
    ]),
    transition('expanded => collapsed', [
        animate('300ms ease-in')
    ])
]);
